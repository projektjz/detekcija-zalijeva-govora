﻿namespace Projekt
{
    partial class GlavnoOkno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnIzdelajUcneMnnozice = new System.Windows.Forms.Button();
            this.btnTestniKorpus = new System.Windows.Forms.Button();
            this.ofdNaloziKorpus = new System.Windows.Forms.OpenFileDialog();
            this.ofdNaloziTekst = new System.Windows.Forms.OpenFileDialog();
            this.btnNaloziUcneMnozice = new System.Windows.Forms.Button();
            this.ofdNaloziUcneMnozice = new System.Windows.Forms.OpenFileDialog();
            this.bwObdelava = new System.ComponentModel.BackgroundWorker();
            this.lbKletvice = new System.Windows.Forms.ListBox();
            this.lblCas = new System.Windows.Forms.Label();
            this.lbNormalne = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btnIzdelajUcneMnnozice
            // 
            this.btnIzdelajUcneMnnozice.Location = new System.Drawing.Point(12, 12);
            this.btnIzdelajUcneMnnozice.Name = "btnIzdelajUcneMnnozice";
            this.btnIzdelajUcneMnnozice.Size = new System.Drawing.Size(161, 23);
            this.btnIzdelajUcneMnnozice.TabIndex = 2;
            this.btnIzdelajUcneMnnozice.Text = "Izdelaj učne množice";
            this.btnIzdelajUcneMnnozice.UseVisualStyleBackColor = true;
            this.btnIzdelajUcneMnnozice.Click += new System.EventHandler(this.btnIzdelajUcneMnnozice_Click);
            // 
            // btnTestniKorpus
            // 
            this.btnTestniKorpus.Location = new System.Drawing.Point(12, 70);
            this.btnTestniKorpus.Name = "btnTestniKorpus";
            this.btnTestniKorpus.Size = new System.Drawing.Size(161, 57);
            this.btnTestniKorpus.TabIndex = 3;
            this.btnTestniKorpus.Text = "Naloži testni korpus in obdelaj";
            this.btnTestniKorpus.UseVisualStyleBackColor = true;
            this.btnTestniKorpus.Click += new System.EventHandler(this.btnTestniKorpus_Click);
            // 
            // ofdNaloziKorpus
            // 
            this.ofdNaloziKorpus.DefaultExt = "*.xml";
            this.ofdNaloziKorpus.Multiselect = true;
            // 
            // ofdNaloziTekst
            // 
            this.ofdNaloziTekst.DefaultExt = "*.txt";
            this.ofdNaloziTekst.Filter = "Text|*.txt";
            this.ofdNaloziTekst.Multiselect = true;
            // 
            // btnNaloziUcneMnozice
            // 
            this.btnNaloziUcneMnozice.Location = new System.Drawing.Point(12, 41);
            this.btnNaloziUcneMnozice.Name = "btnNaloziUcneMnozice";
            this.btnNaloziUcneMnozice.Size = new System.Drawing.Size(161, 23);
            this.btnNaloziUcneMnozice.TabIndex = 4;
            this.btnNaloziUcneMnozice.Text = "Naloži učne množice";
            this.btnNaloziUcneMnozice.UseVisualStyleBackColor = true;
            this.btnNaloziUcneMnozice.Click += new System.EventHandler(this.btnNaloziUcneMnozice_Click);
            // 
            // ofdNaloziUcneMnozice
            // 
            this.ofdNaloziUcneMnozice.DefaultExt = "*.umn";
            this.ofdNaloziUcneMnozice.Filter = "Učna množica|*.umn";
            this.ofdNaloziUcneMnozice.Multiselect = true;
            // 
            // bwObdelava
            // 
            this.bwObdelava.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwObdelava_DoWork);
            // 
            // lbKletvice
            // 
            this.lbKletvice.FormattingEnabled = true;
            this.lbKletvice.Location = new System.Drawing.Point(179, 12);
            this.lbKletvice.Name = "lbKletvice";
            this.lbKletvice.Size = new System.Drawing.Size(546, 277);
            this.lbKletvice.TabIndex = 6;
            // 
            // lblCas
            // 
            this.lblCas.AutoSize = true;
            this.lblCas.Location = new System.Drawing.Point(12, 471);
            this.lblCas.Name = "lblCas";
            this.lblCas.Size = new System.Drawing.Size(0, 13);
            this.lblCas.TabIndex = 7;
            // 
            // lbNormalne
            // 
            this.lbNormalne.FormattingEnabled = true;
            this.lbNormalne.Location = new System.Drawing.Point(179, 295);
            this.lbNormalne.Name = "lbNormalne";
            this.lbNormalne.Size = new System.Drawing.Size(546, 277);
            this.lbNormalne.TabIndex = 8;
            // 
            // GlavnoOkno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(737, 585);
            this.Controls.Add(this.lbNormalne);
            this.Controls.Add(this.lblCas);
            this.Controls.Add(this.lbKletvice);
            this.Controls.Add(this.btnNaloziUcneMnozice);
            this.Controls.Add(this.btnTestniKorpus);
            this.Controls.Add(this.btnIzdelajUcneMnnozice);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "GlavnoOkno";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "N-Grami";
            this.Load += new System.EventHandler(this.GlavnoOkno_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnIzdelajUcneMnnozice;
        private System.Windows.Forms.Button btnTestniKorpus;
        private System.Windows.Forms.OpenFileDialog ofdNaloziKorpus;
        private System.Windows.Forms.OpenFileDialog ofdNaloziTekst;
        private System.Windows.Forms.Button btnNaloziUcneMnozice;
        private System.Windows.Forms.OpenFileDialog ofdNaloziUcneMnozice;
        private System.ComponentModel.BackgroundWorker bwObdelava;
        private System.Windows.Forms.ListBox lbKletvice;
        private System.Windows.Forms.Label lblCas;
        private System.Windows.Forms.ListBox lbNormalne;
    }
}

