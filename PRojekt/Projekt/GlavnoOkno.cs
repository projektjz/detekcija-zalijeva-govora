﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace Projekt
{
    public partial class GlavnoOkno : Form
    {
        private List<string> _nGramiNormalne = new List<string>();
        private List<string> _nGramiKletvice = new List<string>();

        public GlavnoOkno()
        {
            InitializeComponent();
        }

        private void IzdelavaUcneMnozice(string datoteka)
        {
            //Branje
            string tekst = File.ReadAllText(datoteka);

            //Izdelava N-gramov
            List<string> seznamNGramov = IzdelavaNGramov(ref tekst);

            //Zapis N-gramov v datoteko po vrsticah
            File.WriteAllLines(datoteka.Replace(".txt", "") + "_ucna.umn", seznamNGramov.ToArray());
        }

        private List<string> IzdelavaNGramov(ref string besedilo)
        {
            //Slovar N-gramov
            Dictionary<string, int> slovarNGramov = new Dictionary<string, int>();

            //Vse v male črke
            string tekst = besedilo.ToLower();

            //Regex za samo črke
            Regex rgx = new Regex("[^a-zščž ]");
            tekst = rgx.Replace(tekst, "");

            foreach (string beseda in tekst.Split(' '))
            {
                //Besedi dodamo prazne prostore
                string tempBeseda = " " + beseda + " ";

                //Ustvarjanje
                for (int i = 1; i <= 5; i++)
                {
                    int indexZacetka = 0;
                    for (int j = 0; j < tempBeseda.Length; j++)
                    {
                        if (indexZacetka + i < tempBeseda.Length)
                        {
                            string subStr = tempBeseda.Substring(indexZacetka, i);
                            indexZacetka += i;

                            if (slovarNGramov.ContainsKey(subStr))
                                slovarNGramov[subStr]++;
                            else
                                slovarNGramov.Add(subStr, 1);
                        }
                    }
                }
            }

            var urejenSeznam = slovarNGramov.Keys.OrderByDescending(k => slovarNGramov[k]).ToList().Take(400).ToList();

            //Vrnemo 400 najvišjih
            return urejenSeznam;
        }

        private int IzracunRazdaljNormalne(ref List<string> obdelovaniSeznam)
        {
            int razdalje = 0;

            for (int i = 0; i < obdelovaniSeznam.Count; i++)
            {
                int index = _nGramiNormalne.IndexOf(obdelovaniSeznam[i]);

                if (index == -1)
                    razdalje += 400;
                else
                    razdalje += Math.Abs(index - i);
            }

            return razdalje;
        }

        private int IzracunRazdaljKletvice(ref List<string> obdelovaniSeznam)
        {
            int razdalje = 0;

            for (int i = 0; i < obdelovaniSeznam.Count; i++)
            {
                int index = _nGramiKletvice.IndexOf(obdelovaniSeznam[i]);

                if (index == -1)
                    razdalje += 400;
                else
                    razdalje += Math.Abs(index - i);
            }

            return razdalje;
        }

        private void btnTestniKorpus_Click(object sender, EventArgs e)
        {
            DialogResult dResult = ofdNaloziKorpus.ShowDialog();

            if (dResult == DialogResult.OK)
            {
                bwObdelava.RunWorkerAsync();
            }
        }

        private void btnNaloziUcneMnozice_Click(object sender, EventArgs e)
        {
            DialogResult dResult = ofdNaloziUcneMnozice.ShowDialog();

            if (dResult == DialogResult.OK)
            {
                foreach (string ucnaMnozica in ofdNaloziUcneMnozice.FileNames)
                {
                    if (ucnaMnozica.Contains("normalne"))
                        _nGramiNormalne = File.ReadAllLines(ucnaMnozica).ToList();

                    if (ucnaMnozica.Contains("kletvice"))
                        _nGramiKletvice = File.ReadAllLines(ucnaMnozica).ToList();
                }
            }
        }

        private void bwObdelava_DoWork(object sender, DoWorkEventArgs e)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            //Polje niti
            Thread[] poljeNiti = new Thread[ofdNaloziKorpus.FileNames.Length];

            //Za vsako izbrano datoteko
            int indexNiti = 0;
            foreach (string datoteka in ofdNaloziKorpus.FileNames)
            {
                //Uporabimo niti
                poljeNiti[indexNiti] = new Thread(() => ObdelavaDatoteke(datoteka));
                poljeNiti[indexNiti].Start();

                indexNiti++;
            }

            //Počakamo na zaključek vseh niti
            foreach (Thread nit in poljeNiti)
            {
                nit.Join();
            }

            stopwatch.Stop();

            lblCas.Invoke(new MethodInvoker(delegate { lblCas.Text = "Potreben čas:" + stopwatch.Elapsed; }));
        }

        private void ObdelavaDatoteke(string datoteka)
        {
            string tekst = File.ReadAllText(datoteka);

            //Združi vrstice v eno
            tekst = tekst.Replace("\r\n", " ");

            //Vse v male črke
            tekst = tekst.ToLower();

            //Izlušči stavke
            foreach (string stavek in tekst.Split(new [] { '.', '?', '!' }, StringSplitOptions.RemoveEmptyEntries))
            {
                //Regex za samo črke
                Regex rgx = new Regex("[^a-zščž ]");
                string popravjenoBesedilo = rgx.Replace(stavek, "");

                int normalne = 0;
                int kletvice = 0;

                //Razdeli na besede
                foreach (string beseda in popravjenoBesedilo.Split(' '))
                {
                    string tempBeseda = beseda;

                    //Izdelava N-Gramov za besedo
                    List<string> nGrami = IzdelavaNGramov(ref tempBeseda);

                    if (beseda != "")
                    {
                        //Izračuni
                        normalne += IzracunRazdaljNormalne(ref nGrami);
                        kletvice += IzracunRazdaljKletvice(ref nGrami);
                    }
                }

                //Prag
                if (normalne > kletvice)
                {
                    if (Math.Abs(normalne - kletvice) >= 300)
                    {
                        lbKletvice.Invoke(new MethodInvoker(delegate { lbKletvice.Items.Add("KLETVICA: " + popravjenoBesedilo); }));
                    }
                    else
                    {
                        lbNormalne.Invoke(new MethodInvoker(delegate { lbNormalne.Items.Add("NORMALNA: " + popravjenoBesedilo); }));
                    }
                }
                else
                {
                    lbNormalne.Invoke(new MethodInvoker(delegate { lbNormalne.Items.Add("NORMALNA: " + popravjenoBesedilo); }));
                }
            }
        }

        private void btnIzdelajUcneMnnozice_Click(object sender, EventArgs e)
        {
            DialogResult dResult = ofdNaloziTekst.ShowDialog();

            if (dResult == DialogResult.OK)
            {
                foreach (string datoteka in ofdNaloziTekst.FileNames)
                {
                    IzdelavaUcneMnozice(datoteka);
                }
            }
        }

        private void GlavnoOkno_Load(object sender, EventArgs e)
        {

        }
    }
}